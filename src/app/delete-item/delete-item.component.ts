import { Component, OnInit } from '@angular/core';
import {CardSerciceService, Food} from '../shared/card-sercice.service';
import {HttpClient} from '@angular/common/http';
import {DomSanitizer} from '@angular/platform-browser';
import {MatIconRegistry} from '@angular/material';

@Component({
  selector: 'app-delete-item',
  templateUrl: './delete-item.component.html',
  styleUrls: ['./delete-item.component.css']
})
export class DeleteItemComponent implements OnInit {

  public cards: Array<Food> = [];
  item: Array<Food> = [];

  constructor(private service: CardSerciceService, private http: HttpClient, iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
    iconRegistry.addSvgIcon(
      'thumbs-up',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/baseline-delete_forever-24px.svg'));
  }

  ngOnInit() {
    this.service.getData().subscribe((cards: any) => {
      // console.log(data);
      this.cards = cards;
    });
  }

  deleteProduct(id) {
    if (confirm("Sigur doriti sa stergeti?")) {
      let index: any = this.cards;
      this.cards.splice(index, 1);
      this.http.delete('http://localhost:3000/feluri/' + id.toString()).subscribe((dat: any) => {
        console.log('stergere')

      });
    }
  }
}

