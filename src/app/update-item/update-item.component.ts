import { Component, OnInit, ViewChild } from '@angular/core';
import {DataSource} from '@angular/cdk/collections'
import {MatIconRegistry, MatTableDataSource} from '@angular/material';
import {CardSerciceService, Food} from '../shared/card-sercice.service';
import {DomSanitizer} from '@angular/platform-browser';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-update-item',
  templateUrl: './update-item.component.html',
  styleUrls: ['./update-item.component.css']
})
export class UpdateItemComponent implements OnInit {

  public cards: Array <Food> = [];
  curent:Food;
  editMod: Boolean = false;
  den: string;
  pret: number;
  image: string;
  ingrediente: string;

  constructor(private service: CardSerciceService, private http: HttpClient, iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
    iconRegistry.addSvgIcon(
      'thumbs-ups',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/round-border_color-24px.svg'));
  }

  ngOnInit() {
    this.service.getData().subscribe((cards : any ) => {
      // console.log(data);
      this.cards = cards;
    });
  }
  setUpdateMod(f: Food) {
    this.curent = f;
    this.den = f.denumire;
    this.pret = f.pret;
    this.image = f.image;
    this.ingrediente = f.ingrediente;
    this.editMod = true;
  }

  update() {

    this.curent.denumire = this.den;
    this.curent.pret = this.pret;
    this.curent.ingrediente = this.ingrediente;

    if (this.image === '') {
      this.image = 'http://via.placeholder.com/286x179';
    }

    this.curent.image = this.image;

    const data = {
      denumire: this.den,
      pret: this.pret,
      image: this.image,
      ingrediente: this.ingrediente
    }

    this.http.put('http://localhost:3000/feluri/' +  this.curent.id.toString(), data).subscribe(( dat: any ) => {
      console.log('modificat');
    });

    this.editMod = false;
  }


}



