import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dejun'
})
export class DejunPipe implements PipeTransform {

  transform(opt: any, select: any): any {
    console.log('selectat', select);
    return  opt.filter((card)=>  card.denumire === select );
  }
}
