import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchItem'
})
export class SearchItemPipe implements PipeTransform {

  transform(foodItems, searchStr): any {
    if (foodItems.length === 0 || searchStr === '') {
      return foodItems;
    }

    return foodItems.filter((card) => card.denumire.toLowerCase().indexOf(searchStr.toLowerCase()) !== -1);
  }


}
