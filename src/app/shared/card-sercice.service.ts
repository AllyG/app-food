import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
export interface Food {
  id: number;
  denumire: string;
  pret: number;
  image: string;
  tip: string;
  ingrediente: string;
}
@Injectable()
export class CardSerciceService {

 public itemsCos: Array <Food> = [];
 public product: object = {};
 fserv: Food;
 private item = new BehaviorSubject<number>( 35);
  cast = this.item.asObservable();
  addToCar(card) {
    this.itemsCos.push(card);
    this.item.next(5);
  }
  constructor( public http: HttpClient) {
  }
/*---------------------------*/
  getData() {
    return this.http.get('http://localhost:3000/feluri');
  }
  /*---------------------------*/
   postData() {
     const data = {
       denumire: 'hgj',
       pret: 35,
       image: 'hglhghjl',
       ingrediente: 'hgfkhkfhkhv'
     }
    return this.http.post('http://localhost:3000/feluri', data);
  }
}
