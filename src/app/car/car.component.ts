import { Component, OnInit, EventEmitter, Output} from '@angular/core';
import {CardSerciceService, Food} from '../shared/card-sercice.service';
import {MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-car',
  templateUrl: './car.component.html',
  styleUrls: ['./car.component.css']
})
export class CarComponent implements OnInit {

  public itemsCos: Array <Food> = [];
  rez: number = 0;
  constructor(private service: CardSerciceService) {
  }
  /*-------------------------------*/
  ngOnInit() {
    this.service.cast.subscribe((cards ) => {
      this.cost();
      this.sincCos();

    });
  }
  cost () {
    this.rez = 0;
    for (let item of this.itemsCos) {
      this.rez += item.pret;
      console.log(this.rez);
    }

  }
  /*-------------------------------*/
  sincCos() {
    this.itemsCos = this.service.itemsCos;
    console.log(this.itemsCos);
  }
  /*-------------------------------*/

}
