import { Component, OnInit } from '@angular/core';
import { CardSerciceService} from '../shared/card-sercice.service';
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.css']
})
export class AddItemComponent implements OnInit {
    den: string;
    pret: number;
    image: string;
    ingrediente: string;
    confirmare: string = "Un nou produs a fost adaugat";
    adaugat: boolean = false;
  // constructor(private service: CardSerciceService) {
  // }
  constructor(private http : HttpClient) {
  }
  /*******************************/
  addNewProduct() {
    const data = {
      denumire: this.den,
      pret: this.pret,
      image: this.image,
      ingrediente: this.ingrediente
    }
    if (this.image ==='') {
      this.image = 'http://via.placeholder.com/286x179';
    }
    this.http.post('http://localhost:3000/feluri', data).subscribe((dat : any ) => {
      this.adaugat = true;
      console.log('daTE')
      this.den ='';
      this.pret = 0;
      this.image = '';
      this.ingrediente = '';

    });

  }
  /*******************************/
  ngOnInit() {
  }

}
