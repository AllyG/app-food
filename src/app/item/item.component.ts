import { Component, OnInit } from '@angular/core';
import {CardSerciceService, Food} from '../shared/card-sercice.service';
@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {
  search = '';
 // Type = [{name: 'dejun'}, {name: 'prinz'}, {name: 'desert'}];
  selectedType: any = '' //= this.Type[0];
  op :any = this.selectedType
  public cards: Array <Food> = [];
  constructor(private service: CardSerciceService) { }
  ngOnInit() {
    this.service.getData().subscribe((cards : any ) => {
     // console.log(data);
      this.cards = cards;
    });
  }
  // onChange(newObj) {
  //   this.op = newObj;
  //   console.log(this.op);
  //   switch(this.op) {
  //     case 'dejun': {
  //       let str = 'dejun'
  //       this.cards.slice(0, 2);
  //       break;
  //     }
  //     case 'prinz': {
  //       console.log("Good");
  //       break;
  //     }
  //     case 'desert': {
  //       console.log("Fair");
  //       break;
  //     }
  //   }
  // }

  addCar(card: Food) {
    this.service.addToCar(card);
  }
}
