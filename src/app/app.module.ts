import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { CardListComponent } from './card-list/card-list.component';
import { AboutComponent } from './about/about.component';
import { CardSerciceService } from './shared/card-sercice.service';
import { SettingsComponent } from './settings/settings.component';
import { ItemComponent } from './item/item.component';
import { CarComponent } from './car/car.component';
import { AddItemComponent } from './add-item/add-item.component';

import {MatSidenavModule} from '@angular/material/sidenav';
import { MatTableModule } from '@angular/material/table';
import { MatInputModule } from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { MatTabsModule } from '@angular/material/tabs';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { SelectItemsComponent } from './select-items/select-items.component';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatRadioModule} from '@angular/material/radio';
import { UpdateItemComponent } from './update-item/update-item.component';
import { DeleteItemComponent } from './delete-item/delete-item.component';
import { SearchItemPipe } from './shared/search-item.pipe';
import { DejunPipe } from './shared/dejun.pipe';


@NgModule({
  declarations: [
    AppComponent,
    CarComponent,
    CardListComponent,
    SettingsComponent,
    AboutComponent,
    ItemComponent,
    AddItemComponent,
    SelectItemsComponent,
    UpdateItemComponent,
    DeleteItemComponent,
    SearchItemPipe,
    DejunPipe

  ],
  imports: [
    FormsModule,
    BrowserModule,
    MatButtonModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MatToolbarModule,
    MatCardModule,
    MatDividerModule,
    MatListModule,
    MatInputModule,
    MatFormFieldModule,
    MatTableModule,
    MatTabsModule,
    NoopAnimationsModule,
    HttpClientModule,
    MatSidenavModule,
    MatCheckboxModule,
    MatRadioModule,
    MatIconModule

  ],
  providers: [CardSerciceService],
  bootstrap: [AppComponent]
})
export class AppModule { }



