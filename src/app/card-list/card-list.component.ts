import { Component, OnInit } from '@angular/core';
import {CardSerciceService} from '../shared/card-sercice.service';


@Component({
  selector: 'app-card-list',
  templateUrl: './card-list.component.html',
  styleUrls: ['./card-list.component.css']
})
export class CardListComponent implements OnInit {
  constructor() { }
  show: boolean = true;
  ngOnInit() { }
  showElements () {
    this.show = false;
  }

}
